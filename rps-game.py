from random import choice

userScore = 0
compScore = 0

selections = ["rock", "paper", "scissors"]
maxScore = input ("\nType max score (Blank for exit): ")


if maxScore == "":
    print("\nExit by user.")
    exit()


while True == True:
    

    if (userScore or compScore) == int(maxScore):
        break

    compSelection = choice(selections)

    userSelection_Bigger = input ("\n\n\n\nSelect one [(R)ock, (P)aper, (S)cissors] (Blank for exit): ")
    userSelection = userSelection_Bigger.lower()

    
    if userSelection == "r":
        userSelection = "rock"
    elif userSelection == "p":
        userSelection = "paper"
    elif userSelection == "s":
        userSelection = "scissors"
    elif userSelection == "":
        print ("\nExit by user")
        exit()
    else:
        print("\n\nWrong input,\n\n")
    
    print(f"\nYou = {userSelection}, Comp. = {compSelection}\n\n")

    if userSelection == "rock" and compSelection == "scissors":
        print ("You win.")
        userScore += 1
    
    elif userSelection == "rock" and compSelection == "paper":
        print ("Comp. win.")
        compScore += 1
    
    elif userSelection == "rock" and compSelection == "rock":
        print ("Draw.")

    elif userSelection == "paper" and compSelection == "scissors":
        print ("Comp. win.")
        compScore += 1
    
    elif userSelection == "paper" and compSelection == "paper":
        print ("Draw.")

    elif userSelection == "paper" and compSelection == "rock":
        print ("You win.")
        userScore += 1 
    
    elif userSelection == "scissors" and compSelection == "scissors":
        print ("Draw.") 

    elif userSelection == "scissors" and compSelection == "paper":
        print ("You win.")
        userScore += 1 
    
    elif userSelection == "scissors" and compSelection == "rock":
        print ("Comp win.")
        compScore += 1
    
    scoreBoard = f"User = {userScore} | Comp. = {compScore}"
    print(scoreBoard)
if userScore > compScore:
    print (f"\n\n\nUser reached {maxScore},\n--------\nUSER WON !\n--------")
else:
    print (f"\n\n\nComputer reached {maxScore},\n------------\nCOMPUTER WON !\n------------")
