############### TR ###############

Python ile yazılmış "Taş Kağıt Makas" oyunu.

Başlangıçta kazanmak için gereken maxsimum skoru giriniz.

Sonrasında seçimlerinizi Taş = "r" Kağıt = "p" Makas = "s" olarak devam ettirebilirsiniz.

İyi eğlenceler.


############### EN ###############

Python based "Rock Papaer Scissors" game.

When you start the game, type max score you want.

Then make your choices like Rock = "r" Paper = "p" Scissors = "s".

Have fun.
